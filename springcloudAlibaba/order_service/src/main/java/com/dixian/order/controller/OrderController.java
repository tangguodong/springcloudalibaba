package com.dixian.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/add")
    public String place(){
        System.out.println("下单成功");
        ResponseEntity<String> boBy = restTemplate.getForEntity("http://stock-service/stock/reduce", String.class);
        return "下单成功"+boBy.getBody();
    }
}
