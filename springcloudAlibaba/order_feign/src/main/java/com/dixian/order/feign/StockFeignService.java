package com.dixian.order.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "stock-service" ,path="/stock")
public interface StockFeignService {

    @GetMapping("/reduce")
    public String reduceStock();

}
