package com.dixian.order.controller;

import com.dixian.order.feign.StockFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private StockFeignService stockFeignService;



    @GetMapping("/add")
    public String place(){
        System.out.println("下单成功");
        String msg = stockFeignService.reduceStock();
        return "下单成功"+msg;
    }
}
